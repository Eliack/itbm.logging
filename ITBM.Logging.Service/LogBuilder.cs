﻿// <copyright file="LogBuilder.cs" company="Zachary Joubert">
//    Copyright (c) Zachary Joubert All rights reserved.
// </copyright>
namespace ITBM.Logging.Service
{
    using System;
    using System.Text;
    using NLog;

    /// <summary>
    /// The Log Builder class
    /// </summary>
    public class LogBuilder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LogBuilder"/> class.
        /// </summary>
        /// <param name="loggingServiceName">Provides a name for logs to be under.</param>
        public LogBuilder(string loggingServiceName)
        {
            this._logger = LogManager.GetCurrentClassLogger();
            this._loggingServiceName = loggingServiceName;
        }

        #region Private Properties

        /// <summary>
        /// Gets an instance of <see cref="Logger"/>
        /// </summary>
        private Logger _logger { get; }

        /// <summary>
        /// Gets the name of the service doing the logging
        /// </summary>
        private string _loggingServiceName { get; }

        #endregion

        #region Public Methods

        /// <summary>
        /// Adds debug level message to the log
        /// </summary>
        /// <param name="message">The message</param>
        public void Debug(string message)
        {
            message = string.Format("[Service: {0}] Message: {1}", this._loggingServiceName, message);
            this._logger.Debug(message);
        }

        /// <summary>
        /// Adds debug level message to the log
        /// </summary>
        /// <param name="message">The message</param>
        /// <param name="args">The arguments for the message</param>
        public void Debug(string message, params object[] args)
        {
            message = string.Format("[Service: {0}] Message: {1}", this._loggingServiceName, message);
            this._logger.Debug(message, args);
        }

        /// <summary>
        /// Adds info level message to the log
        /// </summary>
        /// <param name="message">The message</param>
        public void Info(string message)
        {
            message = string.Format("[Service: {0}] Message: {1}", this._loggingServiceName, message);
            this._logger.Info(message);
        }

        /// <summary>
        /// Adds info level message to the log
        /// </summary>
        /// <param name="message">The message</param>
        /// <param name="args">The arguments for the message</param>
        public void Info(string message, params object[] args)
        {
            message = string.Format("[Service: {0}] Message: {1}", this._loggingServiceName, message);
            this._logger.Info(message, args);
        }

        /// <summary>
        /// Adds warn level message to the log
        /// </summary>
        /// <param name="message">The message</param>
        public void Warn(string message)
        {
            message = string.Format("[Service: {0}] Message: {1}", this._loggingServiceName, message);
            this._logger.Warn(message);
        }

        /// <summary>
        /// Adds warn level message to the log
        /// </summary>
        /// <param name="message">The message</param>
        /// <param name="args">The arguments for the message</param>
        public void Warn(string message, params object[] args)
        {
            message = string.Format("[Service: {0}] Message: {1}", this._loggingServiceName, message);
            this._logger.Warn(message, args);
        }

        /// <summary>
        /// Adds Error level message to the log
        /// </summary>
        /// <param name="message">The message</param>
        public void Error(string message)
        {
            message = string.Format("[Service: {0}] Message: {1}", this._loggingServiceName, message);
            this._logger.Error(message);
        }

        /// <summary>
        /// Adds Error level message to the log
        /// </summary>
        /// <param name="message">The message</param>
        /// <param name="args">The arguments for the message</param>
        public void Error(string message, params object[] args)
        {
            message = string.Format("[Service: {0}] Message: {1}", this._loggingServiceName, message);
            this._logger.Error(message, args);
        }

        /// <summary>
        /// Adds Error Level message to the Log
        /// </summary>
        /// <param name="message">The message</param>
        /// <param name="exception">The exception</param>
        public void Error(string message, Exception exception)
        {
            message = string.Format("[Service: {0}] Message: {1}, Error: {2}", this._loggingServiceName, message, this.GetLog(exception));
            this._logger.Error(message);
        }

        /// <summary>
        /// Adds Error Level message to the Log
        /// </summary>
        /// <param name="message">The message</param>
        /// <param name="exception">The exception</param>
        /// <param name="args">The arguments for the message</param>
        public void Error(string message, Exception exception, params object[] args)
        {
            message = string.Format("[Service: {0}] Message: {1}, Error: {2}", this._loggingServiceName, message, this.GetLog(exception));
            this._logger.Error(message, args);
        }

        /// <summary>
        /// Adds Fatal level message to the log
        /// </summary>
        /// <param name="message">The message</param>
        public void Fatal(string message)
        {
            message = string.Format("[Service: {0}] Message: {1}", this._loggingServiceName, message);
            this._logger.Fatal(message);
        }

        /// <summary>
        /// Adds Fatal level message to the log
        /// </summary>
        /// <param name="message">The message</param>
        /// <param name="args">The arguments for the message</param>
        public void Fatal(string message, params object[] args)
        {
            message = string.Format("[Service: {0}] Message: {1}", this._loggingServiceName, message);
            this._logger.Fatal(message, args);
        }

        /// <summary>
        /// Adds Fatal level message to the log
        /// </summary>
        /// <param name="message">The message</param>
        /// <param name="exception">The exception</param>
        public void Fatal(string message, Exception exception)
        {
            message = string.Format("[Service: {0}] Message: {1}, Error: {2}", this._loggingServiceName, message, this.GetLog(exception));
            this._logger.Error(message);
        }

        /// <summary>
        /// Adds Fatal Level message to the Log
        /// </summary>
        /// <param name="message">The message</param>
        /// <param name="exception">The exception</param>
        /// <param name="args">The arguments for the message</param>
        public void Fatal(string message, Exception exception, params object[] args)
        {
            message = string.Format("[Service: {0}] Message: {1}, Error: {2}", this._loggingServiceName, message, this.GetLog(exception));
            this._logger.Error(message, args);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Transforms an exception into a log message
        /// </summary>
        /// <param name="exception">An instance of <see cref="Exception"/></param>
        /// <returns>A the log message for the exception</returns>
        private string GetLog(Exception exception)
        {
            var logMessage = new StringBuilder();
            var format = "{2}{0}: {1}";

            if (exception != null)
            {
                logMessage.AppendFormat(format, "Exception: Message", exception.Message, string.Empty);
                logMessage.AppendFormat(format, "Source", exception.Source, ", ");
                logMessage.AppendFormat(format, "StackTrace", exception.StackTrace, ", ");

                var baseException = exception.GetBaseException();
                if (baseException != exception)
                {
                    logMessage.AppendFormat(format, "; Base Exception: Message", baseException.Message, ", ");
                    logMessage.AppendFormat(format, "Base Exception Source", baseException.Source, ", ");
                    logMessage.AppendFormat(format, "Base Exception StackTrace", baseException.StackTrace, ", ");
                }
            }

            return logMessage.ToString();
        }

        #endregion
    }
}